# Hola mundo en C

Este repositorio muestra los elementos básicos que debe tener un proyecto en C para el curso de sistemas operativos.

Cada repositorio debe tener al menos tres archivos:

* `README.md` este es el archivo que da una descripción del proyecto. En este archivo se pueden incluir, por ejemplo, los nombres de los integrantes del grupo, sus correos electrónicos y sus códigos de estudiante.

* `Makefile` este es el programa que contiene las instrucciones básicas para llevar a cabo el proceso de compilación de los códigos de su programa.

* `hola-mundo.c`  en este caso particular, este es el archivo que contiene el codigo fuente de su programa.
